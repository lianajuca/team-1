# CPNT 262 assignment 2 - JavaScript #

Team members:

* Calculator: Liana Jucá (layout), Alex Foster (code), Victor Fan (code)
* Microwave: Greg Mellum (layout), Elias Nahas (code), Tony Lin (code), Victor Fan (code)


Note:

* Microwave is implemented as a state machine (https://en.wikipedia.org/wiki/Finite-state_machine), and the design is in the doc file (microwave_statemachine.jpg)