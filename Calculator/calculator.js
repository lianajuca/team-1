// Declaring global variables
var memory = "0"; // Storage variable
var inputValue = "0"; // In charge of the value that will be displayed and the value that in in the text box.
var operation = 0; // store the operation that is being used IE / * + = etc
var maxLength = 23; // max number of characters user can input
var DEBUG = false; // TODO: set DEBUG to false to disable all debug messages

// Check if a character is +, -, * or /, if so, return true
function isOperator(character) {
    var pattern = /\+|\-|\*|\//g;
    return pattern.test(character);
}
//Function below displays the digit value that is clicked and will store it in the "inputValue" variable.

function displayDigit(digit) {
    if (DEBUG) {
        window.console.log("displayDigit(" + digit + ") , inputValue: >" + inputValue + "< isOperator(digit) >" + isOperator(digit) + "< " + " typeOf(inputValue) >" + typeof (inputValue) + "inputValue == \"0\": " + (inputValue == "0"));
    }

    inputValue = inputValue.toString();

    // Validate the user input is a regular math equation
    if (inputValue.length > maxLength) {
        window.alert("Too many characters"); //limit length
    } else if ((inputValue == "0") && (isOperator(digit))) {
        // Warn user if the equation start with an operator
        window.alert("Should start a math equation with number instead of an operator.");
    } else if ((isOperator(inputValue.slice(-1))) && (isOperator(digit))) {
        // Warn user if two operators is entered such as +*
        window.alert("Two operators in a row.  Operator should be followed by numbers only");
    } else if ((inputValue.slice(-1) == "/") && (digit == "0")) {
        // Warn user if dividing by zero
        window.alert("Divide by zero is illegal");
    } else if ((inputValue == "0") && (inputValue.indexOf(".") == -1)) {
        // Handles decimal
        inputValue = digit;
    } else {
        if (DEBUG) {
            window.console.log("normal append");
        }
        inputValue = inputValue + digit;
    }
    document.calculator.display.value = inputValue;
}



//Function for backspace
// Remove the last character from inputValue and update UI accordinly
function backspace() {
    if (DEBUG) {
        console.log("backspace clicked: inputValue (before): " + inputValue);
    }
    if (inputValue.length > 0) {
        inputValue = inputValue.slice(0, -1);
    }
    if (DEBUG) {
        console.log("backspace clicked: inputValue (after): " + inputValue);
    }
    document.calculator.display.value = inputValue;

}

//Function for Decmials

// Basic explaination:  if the variable inputValue has no contents, then it then it displays a"0." in the display input. if there is already a "." within  the inputValue variable, nothing will happen.

function decimalPoint() {
    if (inputValue.length == 0) {
        inputValue = "0.";
    } else if (inputValue.indexOf(".") == -1) {
        inputValue = inputValue + ".";
    }
    document.calculator.display.value = inputValue;
}

//Function for operators + - / * etc. code here shoudl be pretty easy to understand

function handleMath(operators) {
    if (operators.indexOf("*") > -1) {
        operation = 1;
    }
    if (operators.indexOf("/") > -1) {
        operation = 2;
    }
    if (operators.indexOf("+") > -1) {
        operation = 3;
    }
    if (operators.indexOf("-") > -1) {
        operation = 4;
    }


    if (operators == "/*") {

        alert("Please only use one operator at at time.");
    }
    //Store value in inputValue to the memory variable and apply the selected math operation
    memory = inputValue; //Stores value....
    inputValue = "";
    document.calculator.display.value = inputValue;
}


//Calculate result of the values in inputValue with the value in memory....
function showResult() {
    operation = 0; // Clears the operation variable

    try {
        document.calculator.display.value = eval(inputValue); // display result based on operator selection and InputValue
        inputValue = document.calculator.display.value;
    } catch (err) {
        // Handles invalide input
        document.calculator.display.value = "Invalid input";
    }

    if (DEBUG)
        window.console.log("showResult called input value: " + eval(inputValue));

}

// Clear all entries
function clearAll() {
    memory = "0";
    inputValue = "0";
    document.calculator.display.value = inputValue;
    if (DEBUG) {
        window.console.log("clearAll called: input value (after): " + inputValue);
    }
}

// Display a message if unimplemented button is pressed
function proOnly() {
    var creditCardNum = window.prompt("This function is for Calculator pro only, please enter your credit card number to unlock");
    window.alert("Thanks for purchasing, your unlock code will be mailed to you in " + Math.ceil((Math.random() * 100) + 100) + " years.");
}

// Clear memory
function memoryClear() {
    memory = "0";
    inputValue = "0";
    document.calculator.display.value = inputValue;
}

// MS - Memory store
function memoryStore() {
    memory = Number(eval(document.calculator.display.value));
}

// MR - Memory recall
function memoryRecall() {
    document.calculator.display.value = memory;
    inputValue = memory;
}

// M+ = Add some to Memory
function memoryAddition() {
    memory = Number(memory) + Number(eval(document.calculator.display.value));
}

// M+ = Subtraction some to Memory
function memorySubtraction() {
    memory = Number(memory) - Number(eval(document.calculator.display.value));
}

// C - Clear everything
// CE = Clear error.