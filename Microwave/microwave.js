/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * do NOT run beautify on this file it
 * does not handle jquery / enum correctly
 *!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */

// Define global variables

// intervalID_clock handles the clock
var intervalID_clock;

// intervalID_countdown handles countdown
var intervalID_countdown;

// Update should be less than once a seconds to avoid  time jumping (e.g. from 0:30 to 0:28)
// javascript does not guarantee setInterval will always be accurate
var globalUpdateFrequency = 250;

// Value that will be displayed to the user
var displayValue;

// The minute and second needed for countdown (format MM:SS)
var strCountDownTime = "";

var DEBUG = false; // TODO: set DEBUG to false to disable all debug messages

/* Treat microwave as a state machine
 *          state 0:    display clock
 *          state 1:    user start interacting with num pad 
 *          state 2:    user pressed stop / clear button once
 *          state 3:    user pressed stop / clear button twice
 *          state 4:    user pressed start and start countdown
 *          state 5:    countdown completed
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!! do NOT remove unused state !!!
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
var stateEnum = {
    CLOCK:      0, 
    NUM_PAD:    1, 
    STOP_ONCE:  2, 
    STOP_TWICE: 3, 
    COUNTDOWN:  4,
    DONE:       5    
};

var microwaveState = stateEnum.CLOCK;

// Zero padding for number less than 10
function zeroPadding(num) {
    if (num < 10) {
        return "0" + num;
    } else {
        return num.toString();
    }
}

// When this function is called, it will take one second off from strCountDownTime
function countdownByOneSec() {
    var seconds = 0;
    var minutes = 0;
    var tokens = strCountDownTime.split(":");
    
    if (tokens.length > 1) {
        minutes = parseInt(tokens[0]);
        seconds = parseInt(tokens[1]);
    }
    else {
        seconds = parseInt(tokens[0]);
    }
    
    if (!(seconds <= 0 && minutes <= 0)) {
        if (seconds <= 0) {
            minutes -= 1;
            seconds = 59;
        }
        else {
            seconds -= 1;
        }
    }
    
    if (minutes <= 0) {
        // No need to zero pad if there's no minutes to dispaly
        strCountDownTime = seconds.toString();
    }
    else {
        strCountDownTime = `${zeroPadding(minutes)}:${zeroPadding(seconds)}`;
    }
    if (DEBUG) {
        window.console.log(`minutes: >${zeroPadding(minutes)}< seconds: >${seconds}< strCountDownTime: ${strCountDownTime} microwaveState: >${microwaveState}<`);
    }
    displayValue = strCountDownTime;
}

// Countdown according to user input which maybe some weird time like 99 minutes and 99 seconds
function startCountdown() { 
    intervalID_countdown = window.setInterval(countdownByOneSec, 1000);
}

// Ensure countdown interval is stopped when time is up
function monitorCountdown() {
    // After countdown reach zero, clear the time interval
    if (microwaveState === stateEnum.COUNTDOWN) {
        if (strCountDownTime === "0") {
            clearInterval(intervalID_countdown);
            microwaveState = stateEnum.DONE;
        }
    }
}

// Display the displayValue in txtDisplay text box
function updateDisplay() {
    switch (microwaveState) {
    case stateEnum.CLOCK:
        // By default microwave always display the current HH:MM
        var now = new Date();
        displayValue = zeroPadding(now.getHours()) + ":" + zeroPadding(now.getMinutes());

        if (DEBUG) {
            window.console.log(`state: ${microwaveState} updateDisplay: ${displayValue} strCountDownTime: >${strCountDownTime}<`);
        }
        break;
    case stateEnum.NUM_PAD:
        displayValue = strCountDownTime;
        break;
    case stateEnum.COUNTDOWN:
        if (DEBUG) {
            window.console.log("state: " + microwaveState + " counting down: " + strCountDownTime);
        }
        monitorCountdown();
        if (strCountDownTime === "0") {
            displayValue = "Done!";
        }
        break;
    case stateEnum.STOP_ONCE:
        if (DEBUG) {
            window.console.log("state: " + microwaveState + " counting down: " + strCountDownTime);
        }
        // At this point just display the strCountDownTime
        displayValue = strCountDownTime;
        break;
    }    
    $("#txtDisplay").val(displayValue);
}

// Determine what the user clicked and perform action as a state machine
function processUserInput(key) {
    // When user start interacting with keypad, change state to 1
    if (DEBUG) {
        window.console.log(`User click: >${key}< strCountDownTime: >${strCountDownTime}<`);
    }
    if (/[0-9]/.test(key)) {
        if (DEBUG) {
            window.console.log("\t number: " + key);
        }
        // User cannot enter new time before clicking Stop/Clear again
        if (!(microwaveState === stateEnum.STOP_ONCE || microwaveState === stateEnum.COUNTDOWN)) {
            if (DEBUG) {
                window.console.log(`\t DEBUG: 1 change state from ${microwaveState} to ${stateEnum.NUM_PAD}`);
            }            
            microwaveState = stateEnum.NUM_PAD;

            // Max length = 5 (MM:SS) for safety reason!
            // User should be microwave anything more than 99:99 (99 minutes and 99 seconds) :)
            // Yes, I know it's wierd, but microwave does allow users to set the more than 60 minutes 
            //    and 60 seconds...
            if (strCountDownTime.length < 5) {
                if (strCountDownTime.length === 2) {
                    // Insert a : between the first and second character
                    strCountDownTime = [strCountDownTime.slice(0,1), ":", strCountDownTime.slice(1), key].join("");
                } 
                else if (strCountDownTime.length === 4) {
                    // Since the : is inserted automatically, we need to remove it first and
                    // insert it back to the 3rd position
                    strCountDownTime = strCountDownTime.replace(/:/, '');
                    strCountDownTime = [strCountDownTime.slice(0,2), ":", strCountDownTime.slice(2), key].join("");
                } 
                else {
                    strCountDownTime += key;
                }
            }
        }
    } else if (/Stop\/Clear/.test(key)) {
        if (DEBUG) {
            window.console.log(`\t stop clear: >${key}< strCountDownTime: >${strCountDownTime}`);
        }

        if (microwaveState === stateEnum.NUM_PAD) {
            // The user pressed stop / clear while entering the strCountDownTime, clear everything and go back to clock
            if (DEBUG) {
                window.console.log(`\t DEBUG: 2 change state from ${microwaveState} to ${stateEnum.CLOCK}`);
            }
            microwaveState = stateEnum.CLOCK;
            strCountDownTime = "";
        }
        else if (microwaveState === stateEnum.COUNTDOWN) {
            if (DEBUG) {
                window.console.log(`\t DEBUG: 3 change state from ${microwaveState} to ${stateEnum.STOP_ONCE}`);
            }            
            microwaveState = stateEnum.STOP_ONCE;
            // This is only a pause, only stop countdown
            // no need to clear the strCountDownTime
            clearInterval(intervalID_countdown);
        }
        else if (microwaveState === stateEnum.STOP_ONCE) {
            // Goes back to the clock state and clear the strCountDownTime
            if (DEBUG) {
                window.console.log(`\t DEBUG: 4 change state from ${microwaveState} to ${stateEnum.CLOCK}`);
            }            
            microwaveState = stateEnum.CLOCK;
            strCountDownTime = "";
        }
    } else if (/Start/.test(key)) {
        if (DEBUG) {
            window.console.log("\t start: " + key);
        }
        if (microwaveState === stateEnum.NUM_PAD || microwaveState === stateEnum.STOP_ONCE) {
            if (DEBUG) {
                window.console.log(`\t DEBUG: 5 change state from ${microwaveState} to ${stateEnum.COUNTDOWN}`);
            }                   
            // start count down
            microwaveState = stateEnum.COUNTDOWN;
            startCountdown();
        }
    }
}

// Handles all click events for buttons
$(document).ready(function () {
    $("input").click(function () {
        processUserInput(this.value);
    });
    // Start updating the txtDisplay once the page is fully loaded
    intervalID_clock = window.setInterval(updateDisplay, globalUpdateFrequency);
});