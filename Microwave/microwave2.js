var usrInput = "0";
var txtOutput = "";
var txtDone = "DONE!"
var timeMin = "";
var timeSec = "";
var btnStart = 0;
var btnCancel = 2;
var ctdStart = "";
var ctdIsRunning = 0; // keeps digits from being pressed while countdown is running
var btnPressed = 0;

// function to initialize the microwave clock based on user's system clock
// and to refresh the clock every second
function startClock()
{
    getTime();
    window.setInterval(getTime,1000);
}

function getTime()
{
    // get today's date/time
    var today = new Date();

    var hour = today.getHours();
    var minute = today.getMinutes();

    // zero padding for digits between 0 and 9 for both hours and minutes
    if(hour<10)
    {
        hour = "0" + hour;        
    }

    if(minute<10)
    {
        minute = "0" + minute;        
    }

    // put together a readable time
    var myTime = hour + ":" + minute;
    
    // set our time into the textbox
    // checks to see if any buttons have been pressed, in which case the
    // time is not pushed to the display (though it continues to keep time)
    if (!btnPressed)
    {
        document.getElementById('txtDisplay').value = myTime;
    }

}

// sets the value for each button and calls chkInputLength(btnVal) to store
// it in a variable, and in the case of Start and Stop/Cancel, calls the proper
// functions for the buttons.
function btnPress(btnVal)
{
    switch (btnVal)
        {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                {
                    if (!ctdIsRunning)
                        {
                            chkInputLength(btnVal);
                        };
                }
                break;
            case "Stop/Clear":
                {
                    // check to see if the cancel button has been pressed before
                    // if not, stops the countdown
                    // if so, resets countdown time and display
                    if (btnCancel === 0)
                        {
                            window.clearInterval(ctdStart);
                            btnCancel = 1;
                            btnStart = 0;
                        }
                    else if (btnCancel === 1)
                        {
                            clearAll();
                        }
                }
                break;
            case "Start":
                {
                    // checks to see if value is above "0" for countdown time
                    // and if the start button has been pressed
                    if ((usrInput != "0") && (btnStart === 0))
                        {
                            // calls function to get the minutes and seconds
                            // for countdown. Sets the btnStart and btnCancel
                            // to the proper states and starts the countdown
                            getMinSec();
                            ctdIsRunning = 1;
                            btnStart = 1;
                            btnCancel = 0;
                            ctdStart = window.setInterval(timerCountDown, 1000);
                        }
                }
                break;
        }
}

// determines how to proceed in setting the countdown time
function chkInputLength (valToAppend)
{
    // sets btnPressed to 1 to stop clock from updating the display
    // while we enter the time
    btnPressed = 1;
    if (usrInput.length === 4) // if the user has entered 4 digits already, delete first digit and add new digit to the end
        {
            usrInput = usrInput.substr(1, 4);
        }
    if (usrInput === "0") // if the user hasn't pressed a button or if the user has only pressed "0", do not append the value but rather replace it entirely. Keeps the user from entering leading zeros
        {
            usrInput = valToAppend;
        }
    else
        {
            usrInput += valToAppend; // adds the number entered to the total time
        }
    if (usrInput === "0000")
        {
            usrInput = "0"; // if the user has entered four zeros, ie by entering a non zero digit followed by four zeros, reset the time to one single zero
        }
    // calls function to format the text to be entered in the microwave display
    txtOutFormat();
    // resets the btnCancel so that if a user presses Stop/Cancel before Start, it
    // resets the time, while at the same time preventing the microwave from reseting
    // the clock if no digits have been entered by user.
    btnCancel = 1;
}

function txtOutFormat()
{
    // figures out how to format the text to go into the display with leading zeros
    // for the minutes and seconds if needed.
    switch (usrInput.length)
        {
            case 1:
                {
                    txtOutput = "00:0" + usrInput;
                    break;
                }
            case 2:
                {
                    txtOutput = "00:" + usrInput;
                    break;
                }
            case 3:
                {
                    txtOutput = "0" + usrInput.substr(0, 1) + ":" + usrInput.substr(1, 3);
                    break;
                }
            case 4:
                {
                    txtOutput = usrInput.substr(0, 2) + ":" + usrInput.substr(2, 4);
                }
        }
    // pushes the time that's been input to the microwave display
    document.getElementById("txtDisplay").value = txtOutput;
}

// function to figure out how many minutes and seconds to run the countdown
function getMinSec()
{
    var minSecArray = txtOutput.split(":");
    timeMin = minSecArray[0];
    timeSec = minSecArray[1];
}

// function to clear display and reset everything should the cancel button be pressed
// twice or the countdown reaches zero
function clearAll()
{
    usrInput = "0";
    txtOutput = "";
    timeMin = "";
    timeSec = "";
    btnStart = 0;
    btnCancel = 2;
    document.getElementById("txtDisplay").value = "";
    btnPressed = 0;
    ctdIsRunning = 0;
}

// blinks the display three times to notify user that it is done
// and calls the clearAll function
function done()
{
    function foodDone()
    {
        document.getElementById("txtDisplay").value = txtDone;
    }
    
    function foodDoneBlank()
    {
        document.getElementById("txtDisplay").value = "";
    }
    
    window.clearInterval(ctdStart);
    document.getElementById("txtDisplay").value = "00:00";
    window.setTimeout(foodDone, 1000);
    window.setTimeout(foodDoneBlank, 2000);
    window.setTimeout(foodDone, 3000);
    window.setTimeout(foodDoneBlank, 4000);
    window.setTimeout(foodDone, 5000);
    window.setTimeout(foodDoneBlank, 6000);
    window.setTimeout(clearAll, 7000);
}

// countdown function. Handles conversion from minutes to seconds while counting down
// while at the same time adding leading zeros if need be
function timerCountDown()
{
    if (timeSec == "00")
        {
            if (timeMin > 0)
                {
                    timeMin--;
                    if (timeMin < 10)
                        timeMin = "0" + timeMin;
                    timeSec = "60";
                }
        }
    timeSec--;
    if (timeSec < 10)
        {
            timeSec = "0" + timeSec;
        }
    if ((timeMin == 0) && (timeSec == 0))
        {
            done();
            return;
        }
    
    // format the text to be displayed and refreshes display
    txtOutput = timeMin + ":" + timeSec;
    document.getElementById("txtDisplay").value = txtOutput;
}


// Handles all click events for buttons
$(document).ready(function () {
    $("input[type='button']").click(function () {
        btnPress(this.value);
    });
    // start the microwave clock when everything has been loaded
    startClock();
});